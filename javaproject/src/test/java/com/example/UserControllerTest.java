package com.example;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class UserControllerTest {

    private UserController userController;

    @Before
    public void setUp() throws IOException {
        userController = new UserController(1);
        //This dummy constructor has only 1 user
        //Name= a pass = a
        //owns 1 book id=1
    }

    @Test
    public void testAddToInventory() {
        assertEquals(1, userController.getCurrentUser().getInventory().size());
        userController.getCurrentUser().addToInventory(99);

        List<Integer> inventory = userController.getCurrentUser().getInventory();
        assertEquals(2, inventory.size());
        assertEquals(99, (int) inventory.get(1));
    }
    @Test
    public void testAddToHistory() {
        assertEquals(1, userController.getCurrentUser().getHistory().size());
        userController.getCurrentUser().addToHistory(99);
        List<Integer> history = userController.getCurrentUser().getHistory();
        assertEquals(2, history.size());
        assertEquals(99, (int) history.get(1));
    }

    @Test
    public void testAddToCart() {
        assertEquals(0, userController.getCurrentUser().getCart().size());
        userController.getCurrentUser().addToCart(3);
        List<Integer> cart = userController.getCurrentUser().getCart();
        assertEquals(1, cart.size());
        assertEquals(3, (int) cart.get(0));
    }
    @Test
    public void testDeleteCart() {
        userController.getCurrentUser().addToCart(4);

        // Delete the cart
        userController.getCurrentUser().deleteCart();

        // Check if the cart is empty after deletion
        List<Integer> cart = userController.getCurrentUser().getCart();
        assertEquals(0, cart.size());
    }
    @Test
    public void testGetUsers(){
        List<User> userList = userController.getUsers();
        assertNotNull(userList);
        assertEquals(1, userList.size()); 
    }
    @Test
    public void testGetCurrentUser() {
        User currentUser = userController.getCurrentUser();
        assertNotNull(currentUser);
        assertEquals("a", currentUser.getName()); 
    }
    @Test
    public void testDeleteUser() {
        userController.createUser("b", "b"); //second user
        List<User> initialUsers = userController.getUsers();
        int initialSize = initialUsers.size();
        //deletes user b
        userController.deleteUser(2);
         // Check if the current user is removed from the users list
        List<User> updatedUsers = userController.getUsers();
        int updatedSize = updatedUsers.size();
        assertEquals(initialSize - 1, updatedSize);
        // Delete the current user a
        userController.deleteUser();
        List<User> updatedUsers2 = userController.getUsers();
        int updatedSize2 = updatedUsers2.size();

        assertEquals(updatedSize - 1, updatedSize2); 
    }
    @Test
    public void testTurnAdmin() {
        List<User> initialUsers = userController.getUsers();
        int initialSize = initialUsers.size();
        int currentUserId = userController.getCurrentUser().getId();
        userController.turnAdmin(currentUserId, "AdminRole");
        List<User> updatedUsers = userController.getUsers();
        int updatedSize = updatedUsers.size();

        assertEquals(initialSize, updatedSize);
    }
    @Test
    public void testCreateUser() {
        // Get the initial size of the users list
        List<User> initialUsers = userController.getUsers();
        int initialSize = initialUsers.size();
        userController.createUser("testUser", "testPass");
        List<User> updatedUsers = userController.getUsers();
        int updatedSize = updatedUsers.size();
        assertEquals(initialSize + 1, updatedSize);
        User newUser = updatedUsers.get(updatedSize - 1);
        assertEquals("testUser", newUser.getName());
        assertEquals("testPass", newUser.getPass());
    }

    @Test
    public void testSetCurrentUser() {
        userController.createUser("testUser", "testPass");
        userController.setCurrentUser("testUser", "testPass");
        User currentUser = userController.getCurrentUser();
        assertNotNull(currentUser);
        assertEquals("testUser", currentUser.getName());
        assertEquals("testPass", currentUser.getPass());
    }
    @Test
    public void testModifyUser() {
        User initialUser = userController.getCurrentUser();
        userController.modifyUser("newName", "newPass", "newEmail");

        User modifiedUser = userController.getCurrentUser();
        assertEquals("newName", modifiedUser.getName());
        assertEquals("newPass", modifiedUser.getPass());
        assertEquals("newEmail", modifiedUser.getMail());

        List<User> updatedUsers = userController.getUsers();
        assertEquals("newName", updatedUsers.get(0).getName());
        assertEquals("newPass", updatedUsers.get(0).getPass());
        assertEquals("newEmail", updatedUsers.get(0).getMail());
    }
}