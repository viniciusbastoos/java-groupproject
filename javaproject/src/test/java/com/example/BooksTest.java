package com.example;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class BooksTest {

    /*
     * GETTERS and SETTERS TESTS
     */
    @Test
    public void testGetId() {
        Books ebook = new Ebooks(3, "Title", "Author", "Publisher", 
                "Category", "Description", "Language", 4, 20);
        assertEquals(3, ebook.getId());
    }
    @Test
    public void testSetId() {
        Books ebook = new Ebooks(1, "Title", "Author", "Publisher", 
                "Category", "Description", "Language", 4, 20);

        ebook.setId(3);
        assertEquals(3,ebook.getId());
    }

    @Test
    public void testGetTitle() {
        Books ebook = new Ebooks(1, "Vasco da Gama", "Author", "Publisher", 
                "Category", "Description", "Language", 4, 20);
        assertEquals("Vasco da Gama", ebook.getTitle());
    }
    @Test
    public void testSetTitle() {
        Books ebook = new Ebooks(1, "Hunger Games", "Author", "Publisher", 
                "Category", "Description", "Language", 4, 20);

        ebook.setTitle("Harry Potter");
        assertEquals("Harry Potter",ebook.getTitle());
    }

    @Test
    public void testGetAuthor() {
        Books ebook = new Ebooks(1, "Title", "George Orwell", "Publisher", 
                "Category", "Description", "Language", 4, 20);
        assertEquals("George Orwell", ebook.getAuthor());
    }
    @Test
    public void testSetAuthor() {
        Books ebook = new Ebooks(1, "Title", "J.K. Rowling", "Publisher", 
                "Category", "Description", "Language", 4, 20);

        ebook.setAuthor("George Orwell");
        assertEquals("George Orwell",ebook.getAuthor());
    }

    @Test
    public void testGetPublisher() {
        Books ebook = new Ebooks(1, "Title", "Author", "Montreal Publishers", 
                "Category", "Description", "Language", 4, 20);
        assertEquals("Montreal Publishers", ebook.getPublisher());
    }
    @Test
    public void testSetPublisher() {
        Books ebook = new Ebooks(1, "Title", "Author", "Canada Publishers", 
                "Category", "Description", "Language", 4, 20);

        ebook.setPublisher("Montreal Publishers");
        assertEquals("Montreal Publishers",ebook.getPublisher());
    }
    
    @Test
    public void testGetCategory() {
        Books ebook = new Ebooks(1, "Title", "Author", "Publisher", 
                "Psi-Fi", "Description", "Language", 4, 20);
        assertEquals("Psi-Fi", ebook.getCategory());
    }
    @Test
    public void testSetCategory() {
        Books ebook = new Ebooks(1, "Title", "Author", "Canada Publishers", 
                "Category1", "Description", "Language", 4, 20);

        ebook.setCategory("Montreal Category");
        assertEquals("Montreal Category",ebook.getCategory());
    }


    @Test
    public void testGetDescription() {
        Books ebook = new Ebooks(1, "Title", "Author", "Publisher", 
                "Category", "This book is long", "Language", 4, 20);
        assertEquals("This book is long", ebook.getDescription());
    }
    @Test
    public void testSetDescription() {
        Books ebook = new Ebooks(1, "Title", "Author", "Canada Publishers", 
                "Category1", "Description1", "Language", 4, 20);

        ebook.setDescription("Montreal Description");
        assertEquals("Montreal Description",ebook.getDescription());
    }

    @Test
    public void testGetLanguage() {
        Books ebook = new Ebooks(1, "Title", "Author", "Publisher", 
                "Category", "Description", "Portuguese", 4, 20);
        assertEquals("Portuguese", ebook.getLanguage());
    }
    @Test
    public void testSetLanguage() {
        Books ebook = new Ebooks(1, "Title", "Author", "Canada Publishers", 
                "Category1", "Description", "Language1", 4, 20);

        ebook.setLanguage("Montreal Language");
        assertEquals("Montreal Language",ebook.getLanguage());
    }
    @Test
    public void testGetRating() {
        Books ebook = new Ebooks(1, "Title", "Author", "Publisher", 
                "Category", "Description", "Language", 5, 20);
        assertEquals(5, ebook.getRating());
    }
    @Test
    public void testSetRating() {
        Books ebook = new Ebooks(1, "Title", "Author", "Canada Publishers", 
                "Category1", "Description", "Language1", 4, 20);

        ebook.setRating(1);
        assertEquals(1,ebook.getRating());
    }
    
    @Test
    public void testGetPrice() {
        Books ebook = new Ebooks(1, "Title", "Author", "Publisher", 
                "Category", "Description", "Language", 4, 150);
        assertEquals(150, ebook.getPrice());
    }
    @Test
    public void testSetPrice() {
        Books ebook = new Ebooks(1, "Title", "Author", "Canada Publishers", 
                "Category1", "Description", "Language1", 4, 20);

        ebook.setPrice(30);
        assertEquals(30,ebook.getPrice());
    }

    @Test
    public void testGetDuration() {
        AudioBooks audioBook = new AudioBooks(1, "Title", "Author", "Publisher", 
                "Category", "Description", "Language", 4, 150, 20, "Vinicius");
        assertEquals(20, audioBook.getDuration());
    }
    @Test
    public void testSetDuration() {
        AudioBooks audioBook = new AudioBooks(1, "Title", "Author", "Publisher", 
                "Category", "Description", "Language", 4, 150, 20, "Vinicius");

        audioBook.setDuration(30);
        assertEquals(30,audioBook.getDuration());
    }

    @Test
    public void testGetNarrator() {
        AudioBooks audioBook = new AudioBooks(1, "Title", "Author", "Publisher", 
                "Category", "Description", "Language", 4, 150, 20, "Vinicius");
        assertEquals("Vinicius", audioBook.getNarrator());
    }
    @Test
    public void testSetNarrator() {
        AudioBooks audioBook = new AudioBooks(1, "Title", "Author", "Publisher", 
                "Category", "Description", "Language", 4, 150, 20, "Vinicius");

        audioBook.setNarrator("Chiril");
        assertEquals("Chiril",audioBook.getNarrator());
    }


    /*
     * TO STRING TEST
     */
    @Test
    public void testToString() {
        Ebooks ebook = new Ebooks(10, "Vasco da Gama", "J.K. Rowling", "Montreal Publishers", 
                "Psi-Fi", "This book is long", "Portuguese", 4, 150);

        assertEquals("10,Vasco da Gama,J.K. Rowling,Montreal Publishers,Psi-Fi,This book is long,Portuguese,4,150;", ebook.toString());
    }


    /*
     * COMPARATOR TESTS
     */

    /*
     * < 0 tells me that ebook1 comes before ebook2 
     * > 0 ebook2 comes before ebook1
     */

    // testIdComparator() tests 1 and 2 for IDs
    @Test
    public void testIdComparator() {
        Ebooks ebook1 = new Ebooks(1, "Title1", "Author1", "Publisher1", 
                "Category1", "Description1", "Language1", 4, 20);
        Ebooks ebook2 = new Ebooks(2, "Title2", "Author2", "Publisher2", 
                "Category2", "Description2", "Language2", 4, 20);

        assertTrue(Ebooks.idComparator.compare(ebook1, ebook2) < 0);
        assertTrue(Ebooks.idComparator.compare(ebook2, ebook1) > 0);
    }


    // testTitleComparator1() tests Atitle and Ztitle (letters)
    @Test
    public void testTitleComparator1() {
        Ebooks ebook1 = new Ebooks(1, "Atitle", "Author1", "Publisher1", 
                "Category1", "Description1", "Language1", 4, 20);
        Ebooks ebook2 = new Ebooks(2, "Ztitle", "Author2", "Publisher2", 
                "Category2", "Description2", "Language2", 4, 20);

        assertTrue(Ebooks.titleComparator.compare(ebook1, ebook2) < 0);
        assertTrue(Ebooks.titleComparator.compare(ebook2, ebook1) > 0);    
    }
    // testTitleComparator2() tests Title1 and Ttile2 (numbers)
    @Test
    public void testTitleComparator2() {
        Ebooks ebook1 = new Ebooks(1, "Title1", "Author1", "Publisher1", 
                "Category1", "Description1", "Language1", 4, 20);
        Ebooks ebook2 = new Ebooks(2, "Title2", "Author2", "Publisher2", 
                "Category2", "Description2", "Language2", 4, 20);

        assertTrue(Ebooks.titleComparator.compare(ebook1, ebook2) < 0);
        assertTrue(Ebooks.titleComparator.compare(ebook2, ebook1) > 0);    
    }


    // testAuthorComparator1() tests Author and Bauthor (letters)
    @Test
    public void testAuthorComparator1() {
        Ebooks ebook1 = new Ebooks(1, "Title1", "Aauthor", "Publisher1", 
                "Category1", "Description1", "Language1", 4, 20);
        Ebooks ebook2 = new Ebooks(2, "Title2", "Bauthor", "Publisher2", 
                "Category2", "Description2", "Language2", 4, 20);

        assertTrue(Ebooks.authorComparator.compare(ebook1, ebook2) < 0);
        assertTrue(Ebooks.authorComparator.compare(ebook2, ebook1) > 0);    
    }    
    // testAuthorComparator1() tests Author1 and Author2 (numbers)
    @Test
    public void testAuthorComparator2() {
        Ebooks ebook1 = new Ebooks(1, "Title1", "Author1", "Publisher1", 
                "Category1", "Description1", "Language1", 4, 20);
        Ebooks ebook2 = new Ebooks(2, "Title2", "Author2", "Publisher2", 
                "Category2", "Description2", "Language2", 4, 20);

        assertTrue(Ebooks.authorComparator.compare(ebook1, ebook2) < 0);
        assertTrue(Ebooks.authorComparator.compare(ebook2, ebook1) > 0);    
    }


    // testRatingComparator() tests 4 and 3 for rating
    @Test
    public void testRatingComparator() {
        Ebooks ebook1 = new Ebooks(1, "Title1", "Author1", "Publisher1", 
                "Category1", "Description1", "Language1", 4, 20);
        Ebooks ebook2 = new Ebooks(2, "Title2", "Author2", "Publisher2", 
                "Category2", "Description2", "Language2", 3, 25);

        assertTrue(Ebooks.ratingComparator.compare(ebook1, ebook2) > 0);
        assertTrue(Ebooks.ratingComparator.compare(ebook2, ebook1) < 0);    
    }

    // testRatingComparator() tests 20 and 25 for price
    @Test
    public void testPriceComparator() {
        Ebooks ebook1 = new Ebooks(1, "Title1", "Author1", "Publisher1", 
                "Category1", "Description1", "Language1", 4, 20);
        Ebooks ebook2 = new Ebooks(2, "Title2", "Author2", "Publisher2", 
                "Category2", "Description2", "Language2", 4, 25);
    
        assertTrue(Ebooks.priceComparator.compare(ebook1, ebook2) < 0);
        assertTrue(Ebooks.priceComparator.compare(ebook2, ebook1) > 0);    
    }
    
}
