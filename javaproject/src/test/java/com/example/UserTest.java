package com.example;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

public class UserTest {

    @Test
    public void testCheckAdmin() {
        ArrayList<Integer> books = new ArrayList<Integer>();
        books.add(1);
        User regularUser = new Client(1, "Client", "pass", "@", 10, books, books);
        User adminUser = new Admin(2, "Admin", "adminpass", "@", 20, books, books, 101, "Do things");
        assertFalse(regularUser.checkAdmin());
        assertTrue(adminUser.checkAdmin());
    }

    @Test
    public void testDiscountPrice() {
        ArrayList<Integer> books = new ArrayList<Integer>();
        books.add(1);
        User user = new Client(1, "User", "pass", "email", 500, books,books);
        assertEquals(50, user.discountPrice(100)); // 50% off when points >= 500

        User user2 = new Client(1, "User", "pass", "email", 400, books,books);
        assertEquals(100, user2.discountPrice(100)); // No discount when points below 500
    }

    @Test
    public void testClientConstructor() {
        ArrayList<Integer> inventory = new ArrayList<>();
        inventory.add(1);
        inventory.add(2);
        ArrayList<Integer> history = new ArrayList<>();
        history.add(3);
        history.add(4);

        Client client = new Client(1, "Client", "clientpass", "mail", 20, inventory, history);
        assertEquals(1, client.getId());
        assertEquals("Client", client.getName());
        assertEquals("clientpass", client.getPass());
        assertEquals("mail", client.getMail());
        assertEquals(20, client.getPoints());
        assertEquals(inventory, client.getInventory());
        assertEquals(history, client.getHistory());
    }

    @Test
    public void testAdminConstructor() {
        ArrayList<Integer> inventory = new ArrayList<>();
        inventory.add(5);
        inventory.add(6);
        ArrayList<Integer> history = new ArrayList<>();
        history.add(7);
        history.add(8);

        Admin admin = new Admin(2, "Admin", "adminpass", "mail", 30, inventory, history, 201, "role");
        assertEquals(2, admin.getId());
        assertEquals("Admin", admin.getName());
        assertEquals("adminpass", admin.getPass());
        assertEquals("mail", admin.getMail());
        assertEquals(30, admin.getPoints());
        assertEquals(inventory, admin.getInventory());
        assertEquals(history, admin.getHistory());
        assertEquals(201, admin.getAdmin());
        assertEquals("role", admin.getRole());
    }
    @Test
    public void testClientGettersAndSetters() {
        ArrayList<Integer> books = new ArrayList<Integer>();
        books.add(1);
        Client client = new Client(1, "Client", "clientpass", "mail", 20, books, books);

        //getters
        assertEquals(1, client.getId());
        assertEquals("Client", client.getName());
        assertEquals("clientpass", client.getPass());
        assertEquals("mail", client.getMail());
        assertEquals(20, client.getPoints());
        assertEquals(1, client.getInventory().size());
        assertEquals(1, client.getHistory().size());

        //setters
        client.setName("NewClient");
        client.setPass("newpass");
        client.setEmail("badnjanfi@");
        client.winPoints(); //adds +2pts

        //Verify the changes after the setters
        assertEquals("NewClient", client.getName());
        assertEquals("newpass", client.getPass());
        assertEquals("badnjanfi@", client.getMail());
        assertEquals(22, client.getPoints());
    }

    @Test
    public void testAdminGettersAndSetters() {
        ArrayList<Integer> books = new ArrayList<Integer>();
        books.add(1);
        Admin admin = new Admin(2, "Admin", "adminpass", "mail", 30, books, books, 201, "role");

        //getters
        assertEquals("Admin", admin.getName());
        assertEquals("adminpass", admin.getPass());
        assertEquals("mail", admin.getMail());
        assertEquals(30, admin.getPoints());
        assertEquals(1, admin.getInventory().size());
        assertEquals(1, admin.getHistory().size());
        assertEquals(201, admin.getAdmin());
        assertEquals("role", admin.getRole());

        //setters
        admin.setName("NewAdmin");
        admin.setPass("newadminpass");
        admin.setEmail("newadmin@example.com");
        admin.winPoints();

        // Verify the changes after setters
        assertEquals("NewAdmin", admin.getName());
        assertEquals("newadminpass", admin.getPass());
        assertEquals("newadmin@example.com", admin.getMail());
        assertEquals(32, admin.getPoints());
    }

    @Test
    public void testClientToString() {
        ArrayList<Integer> inventory = new ArrayList<>();
        inventory.add(1);
        inventory.add(2);
        Client client = new Client(1, "Client", "clientpass", "mail", 20, inventory, inventory);
        String expectedToString = "1;Client;clientpass;mail;20;1,2;1,2";
        assertEquals(expectedToString, client.toString());
    }

    @Test
    public void testAdminToString() {
        ArrayList<Integer> inventory = new ArrayList<>();
        inventory.add(5);
        inventory.add(6);
        Admin admin = new Admin(2, "Admin", "adminpass", "mail", 30, inventory, inventory, 201, "role");
        String expectedToString = "2;Admin;adminpass;mail;30;5,6;5,6;201;role";
        assertEquals(expectedToString, admin.toString());
    }
    
}