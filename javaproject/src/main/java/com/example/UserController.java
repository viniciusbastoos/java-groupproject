package com.example;
import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;

public class UserController{
    private List<User> users;   //list of Users
    private User curUser;       //the current connected User object
    
    public UserController() throws IOException{    //This constructor connects a session (User's account)
        FileDataLoader fl = new FileDataLoader();
        users = fl.loadListOfUsers(); //loads all users from users.txt
    }
    public UserController(int x){//dummy constructor
        users = new ArrayList<>();
        createUser("a", "a");
        setCurrentUser("a", "a");
    } 
    public void setCurrentUser(String name, String pass) {
        User foundUser = null;
        for (User u : users) {
            if (u.getName().equals(name) && u.getPass().equals(pass)) {
                foundUser = u;
                break;
            }
        }
        if (foundUser == null) {
            BookstoreApplication.connectSession();
        } else {
            curUser = foundUser;
        }
    }
    public void createUser(String name, String pass) {
        int id = 1;
        while (true) {
            boolean idTaken = false;
            for (User u : this.users) {
                if (u.getId() == id) {
                    idTaken = true;
                    break;
                }
            }
            if (!idTaken) {
                break;
            }
            id++;
        }
        ArrayList<Integer> inventory = new ArrayList<>();
        ArrayList<Integer> history = new ArrayList<>();
        history.add(1);
        inventory.add(1);
        users.add(new Client(id, name, pass, null, 50, inventory, history));
    }
    public void modifyUser(String name, String pass, String email){
        int index = 0;
        for(User u : users){ //checks the index of the correct id
            if(u.getId() == curUser.getId()){
                break;
            }
            index++;
        }
        //add more if points or fees
        curUser.setName(name);
        curUser.setPass(pass);
        curUser.setEmail(email);
        users.set(index,curUser); //assigns the modification into the List
    }
    public void deleteUser(){ //deletes current user from List (Self-Destruct)
        users.remove(curUser);
    }
    public void deleteUser(int id){  //deletes specific User from List
        int index = 0;
        for(User u : users){ //checks the index of the correct id
            if(u.getId() == id){
                break;
            }
            index++;
        }
        users.remove(index);
    }
    public void turnAdmin(int cid,String role){
        int id = 1; //new id
        for(User u : this.users){
            if(u.getId() == id){
                id++;
            }
        }
        int index = 0;
        for(User u : users){ //checks the index of the correct id
            if(u.getId() == cid){
                break;
            }
            index++;
        }
        User u = users.get(index);
        Admin a = new Admin(cid, u.getName(),u.getPass(),u.getMail(),u.getPoints(),u.getInventory(),u.getHistory(),id,role);
        users.set(index,a);
    }
    public User getCurrentUser(){return this.curUser;}
    public List<User> getUsers(){return this.users;}
}
