package com.example;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class FileDataLoader implements IDataLoader{
    private String userpath;
    private String bookpath;
    public FileDataLoader(){ //detects if the os is a mac or windows to fix the path
        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("mac")) {
            userpath = "javaproject/data/users.txt";
            bookpath = "javaproject/data/books.txt";
        } else if (os.contains("win")) {
            userpath = "javaproject\\data\\users.txt";
            bookpath = "javaproject\\data\\books.txt";
        }
    }
    
    public List<Books> loadListOfBooks() throws IOException {
        Path path = Paths.get("javaproject/data/books.txt");
        List<String> allLines = Files.readAllLines(path);
        List<Books> allBooksList = new ArrayList<>();
    
        for (String b : allLines) {
            String[] booksSplitByIndex = b.split(";");

            List<Books> bookFromLine = new ArrayList<>(); //Store books from a single line cause idk how many fields there will be.
    
            for (String bf : booksSplitByIndex) {
                String[] bookFields = bf.split(",");
            if (bookFields.length != 9 && bookFields.length != 11) {
                throw new RuntimeException("Invalid file format. The program will terminate due to this line on books.txt: " + b);
            }
    
                int id = Integer.parseInt(bookFields[0].trim());
                String title = bookFields[1].trim();
                String author = bookFields[2].trim();
                String publisher = bookFields[3].trim();
                String category = bookFields[4].trim();
                String description = bookFields[5].trim();
                String language = bookFields[6].trim();
                int rating = Integer.parseInt(bookFields[7].trim());
                int price = Integer.parseInt(bookFields[8].trim());

                if(bookFields.length == 9){
                    Books book = new Ebooks(id, title, author, publisher, category, description, language, rating, price);
                    bookFromLine.add(book); // for each line of books separated by ";" I had the newly created book to the arrayList

                }
                else if (bookFields.length == 11) {
                    int duration = Integer.parseInt(bookFields[9].trim());
                    String narrator = bookFields[10].trim();
                    Books book = new AudioBooks(id, title, author, publisher, category, description, language, rating, price, duration, narrator);
                    bookFromLine.add(book); // for each line of books separated by ";" I had the newly created book to the arrayList
                }
            }
            // Assuming you want to add all books from a single line to the main list
            allBooksList.addAll(bookFromLine);
        }
        
        return allBooksList;
    }
    
    /*
     * This method is to write the new book to the books.txt
     */
    @Override
    public void saveListOfBooks(List<Books> booksList) throws IOException {
        // Specify the file path
        File file = new File("javaproject/data/books.txt");

        // I saw that BufferedWriter gives better performance
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, true))) {

            for (Books book : booksList) {
                // APPEND each books information to the file
                writer.newLine(); 
                writer.write(book.toString());
            }
        }

    }

    @Override
    public ArrayList<User> loadListOfUsers() throws IOException {
       Path p = Paths.get(this.userpath);
        List<String> lines = Files.readAllLines(p);
        ArrayList<User> users = new ArrayList<User>();
        for(String line : lines){
            if(line == null){
                break;
            }
            String[] fields = line.split(";");
            if (fields.length < 7) {
                throw new RuntimeException("Invalid file format.");
            }
            ArrayList<Integer> books = new ArrayList<Integer>(); 
            String[] bookids = fields[5].split(",");
            for(String ids:bookids){
                books.add(Integer.parseInt(ids));
            }
            ArrayList<Integer> history = new ArrayList<Integer>(); 
            String[] historybookids = fields[6].split(",");
            for(String ids:historybookids){
                history.add(Integer.parseInt(ids));
            }
            if(fields.length == 9){//If Admin,...Admin Object created
                users.add(new Admin(Integer.parseInt(fields[0]),fields[1],fields[2],
                fields[3],Integer.parseInt(fields[4]),
                books,history,Integer.parseInt(fields[7]),fields[8]));
            }else{ //If Client,... Client Object created
            users.add(new Client(Integer.parseInt(fields[0]),fields[1],fields[2],
                fields[3],Integer.parseInt(fields[4]),books,history));
            }
        }
        return users;
    }

    @Override
    public void saveListOfUsers(List<User> users) throws IOException {
        Path p = Paths.get(userpath);
        List<String> userLines = new ArrayList<>();
        for (User user : users) {
            userLines.add(user.toString());
        }
        Files.write(p, userLines, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
    }
    
    public void updateTextFile(List<Books> booksList) throws IOException {
        File file = new File("javaproject/data/books.txt");
    
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, false))) {
            int size = booksList.size(); //Get the size of the list to check whats the last index

            for (int i = 0; i < size; i++) {
                Books book = booksList.get(i);
                writer.write(book.toString());
                if (i < size - 1) {
                    writer.newLine();
                }
            }
        }
    }
    
}
