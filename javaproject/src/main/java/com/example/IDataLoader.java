package com.example;

import java.io.IOException;
import java.util.List;

public interface IDataLoader {
    public List<Books> loadListOfBooks() throws IOException;
    public void saveListOfBooks(List<Books> booksList) throws IOException;
    public List<User> loadListOfUsers() throws IOException;
    public void saveListOfUsers(List<User> users) throws IOException;
}
