// we gotta do modifyAccount
//

package com.example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class BookstoreApplication 
{
    public static UserController uc;
    public static InventoryController in;
    public static FileDataLoader fdl;
    public static void main( String[] args ) throws IOException{
        uc = new UserController();
        in = new InventoryController();
        fdl = new FileDataLoader();
        newUser(); //new account or not
        connectSession(); //login
        uc.getCurrentUser().winPoints(); //Every login, User gets points
        menu(); //mainmenu
        fdl.saveListOfUsers(uc.getUsers()); //saves when user exits
    }
    public static void logo(){
        System.out.println("\u001b[2J\u001b[H");
        System.out.println("  ____  __ __   ____  _      ____  ____   ____    ____  ____   __ __ \r\n" + //
                " |    ||  |  | /    || |    |    ||    \\ |    \\  /    ||    \\ |  |  |\r\n" + //
                " |__  ||  |  ||  o  || |     |  | |  o  )|  D  )|  o  ||  D  )|  |  |\r\n" + //
                " __|  ||  |  ||     || |___  |  | |     ||    / |     ||    / |  ~  |\r\n" + //
                "/  |  ||  :  ||  _  ||     | |  | |  O  ||    \\ |  _  ||    \\ |___, |\r\n" + //
                "\\  `  | \\   / |  |  ||     | |  | |     ||  .  \\|  |  ||  .  \\|     |\r\n" + //
                " \\____j  \\_/  |__|__||_____||____||_____||__|\\_||__|__||__|\\_||____/ \r\n" + //
                "                                                                     ");
    }
    public static void menu() throws IOException{ //(v3) Ui to see the mainmenu
        boolean isValidInput = false;
        while (!isValidInput) {
            System.out.println();
            System.out.println("Welcome " + uc.getCurrentUser().getName() + "!");
            System.out.println("+----------------+\r\n" + //
                    "|  1    Profile  |\r\n" + //
                    "|  2    Library  |\r\n" + //
                    "|  3     Admin   |\r\n" + //
                    "|  4     Exit    |\r\n" + //
                    "+----------------+");
            Scanner k = new Scanner(System.in);
            System.out.print("Enter your choice: ");
            String userInput = k.nextLine();
            switch (userInput) {
                case "1":
                    System.out.println("\u001b[2J\u001b[H");
                    System.out.println("You selected Profile.");
                    isValidInput = true;
                    profileMenu();
                    break;
                case "2":
                    System.out.println("\u001b[2J\u001b[H");
                    System.out.println("You selected Library.");
                    isValidInput = true;
                    libraryMenu();
                    break;
                case "3":
                    if (!uc.getCurrentUser().checkAdmin()) {
                        System.out.println("The current user connected to this session doesn't have Admin rights.");
                        k.nextLine(); // Wait for user input to continue
                        isValidInput = false;
                    } else {
                        System.out.println("\u001b[2J\u001b[H");
                        System.out.println("You selected Admin.");
                        isValidInput = true;
                        inventoryMenu();
                    }
                    break;
                case "4":
                    System.out.println("Exiting program. Goodbye!");
                    fdl.saveListOfUsers(uc.getUsers());
                    System.exit(0);

                    break;
                default:
                    System.out.println("Invalid option. Please enter a number between 1 and 4.");
            }
        }
    }
    public static void profileMenu() throws IOException {
        boolean exitProfileMenu = false;
        while (!exitProfileMenu) {
            System.out.println();
            System.out.println("              Profile\r\n" +
                    "+---------------------------------+\r\n" +
                    "|  1          Inventory           |\r\n" +
                    "|  2        Shopping Cart         |\r\n" +
                    "|  3        Modify Account        |\r\n" +
                    "|  4        Delete Account        |\r\n" +
                    "|  5           Go Back            |\r\n" +
                    "+---------------------------------+");
            System.out.println("User: " + uc.getCurrentUser().getName() + " Points: " + uc.getCurrentUser().getPoints());
            Scanner k = new Scanner(System.in);
            System.out.print("Enter your choice: ");
            String userInput = k.nextLine();
            switch (userInput) {
                case "1":
                    System.out.println("\u001b[2J\u001b[H");
                    exitProfileMenu = true;
                    fdl.loadListOfUsers(); //resets because the user will want to see the cur inventory
                    inventory();
                    break;
                case "2":
                    System.out.println("\u001b[2J\u001b[H");
                    exitProfileMenu = true;
                    cartMenu();
                    break;
                case "3":
                    System.out.println("\u001b[2J\u001b[H");
                    System.out.println("You selected Modify Account.");
                    modifyAccount();
                    break;
                case "4":
                    System.out.println("\u001b[2J\u001b[H");
                    System.out.println("Are you sure you want to delete this account? (y) for yes or (other) for no");
                    String answer = k.next();
                    if(answer.matches("y")){
                        exitProfileMenu = true;
                        uc.deleteUser();
                        fdl.saveListOfUsers(uc.getUsers());
                        break;
                    }
                    break;
                case "5":
                    System.out.println();
                    System.out.println("Going back to the Main Menu.");
                    exitProfileMenu = true;
                    menu();
                    break;
                default:
                    exitProfileMenu = false;
                    System.out.println("Invalid option. Please enter a number between 1 and 5.");
            }
        }
    }

    private static void modifyAccount() throws IOException {
        Scanner k = new Scanner(System.in);
        System.out.println("Name: ");
        String name = k.next();
        boolean bool = false;
        for(User u : uc.getUsers()){
            if(u.getName().equals(name)){
                bool = true;             
            }
        }
        if(bool){
            System.out.println("Name already taken.");
            return;
        }
        System.out.println("Password: ");
        String pass = k.next();
        System.out.println("Email: ");
        String email = k.next();
        uc.modifyUser(name, pass, email);
        fdl.saveListOfUsers(uc.getUsers()); //saves file
    }
    public static void libraryMenu() throws IOException {
        Scanner scan = new Scanner(System.in);
    
        System.out.println();
        System.out.println("What would you like to do?");
        System.out.println("(1) Filter Books List");
        System.out.println("(2) Add to Cart");
        System.out.println("(3) Exit");
    
        String input = scan.nextLine();
    
        while (!input.matches("[1-3]")) {
            System.out.println("Invalid choice. Please enter a valid option.");
            input = scan.nextLine();
        }
        int choice = Integer.parseInt(input);
        switch (choice) {
            case 1:
                filterBooksList();
                libraryMenu();
                break;
            case 2:
                List<Books> allBooksList = fdl.loadListOfBooks(); 
                addToCart(allBooksList);
                libraryMenu();
                break;
            case 3:
                System.out.println("Exiting...");
                menu();
                break;
        }
    }
    
    public static void filterBooksList() throws IOException {
        Scanner scan = new Scanner(System.in);
        FileDataLoader fdl = new FileDataLoader();
        List<Books> books = fdl.loadListOfBooks();
    
        System.out.println();
        System.out.println("How would you like to filter the books list?");
        System.out.println("(1) Sort by ID");
        System.out.println("(2) Sort by Title");
        System.out.println("(3) Sort by Author");
        System.out.println("(4) Sort by Rating");
        System.out.println("(5) Sort by Price");
        System.out.println("(6) Exit");
    
        String input = scan.nextLine();
    
        while (!input.matches("[1-6]")) {
            System.out.println("Invalid choice. Please enter a valid option.");
            input = scan.nextLine();
        }
        int choice = Integer.parseInt(input);
        switch (choice) {
            case 1:
                System.out.println("Sorting books by ID...");
                Collections.sort(books, Books.idComparator);
                printBooksListFiltered(books);
                filterBooksList();
            case 2:
                System.out.println("Sorting books by title...");
                Collections.sort(books, Books.titleComparator);
                printBooksListFiltered(books);
                filterBooksList();
                break;
            case 3:
                System.out.println("Sorting books by author...");
                Collections.sort(books, Books.authorComparator);
                printBooksListFiltered(books);
                filterBooksList();
                break;
            case 4:
                System.out.println("Sorting books by rating...");
                Collections.sort(books, Books.ratingComparator);
                printBooksListFiltered(books);
                filterBooksList();
                break;
            case 5:
                System.out.println("Sorting books by price...");
                Collections.sort(books, Books.priceComparator);
                printBooksListFiltered(books);
                filterBooksList();
                break;
            case 6:
                System.out.println("Exiting to the Filter Menu...");
                libraryMenu();
                break;
        }
    }
    
    public static void addToCart(List<Books> allBooksList) throws IOException {
        Scanner scan = new Scanner(System.in);

        boolean bookFound = false;
    
        System.out.println("Enter the ID of the book you want to add to the cart:");
    
        while (!scan.hasNextInt()) {
            System.out.println("Invalid input. Please enter a valid integer ID.");
            scan.next(); // Consume INVALID input
        }
        int bookID = scan.nextInt();
<<<<<<< HEAD
        scan.nextLine();
=======
        scan.nextLine(); // Consume the newline character
    
>>>>>>> e5389db65d78268f24e08513028003e0e8d16233
        for (Books book : allBooksList) {
            if (book.getId() == bookID) {
                uc.getCurrentUser().addToCart(bookID);
                System.out.println();
                System.out.println("Book added to the cart!");
                System.out.println(uc.getCurrentUser().cartString(in));
                bookFound = true;
                break;
            }
        }
        if (!bookFound) {
            System.out.println();
            System.out.println("This ID does not exist. Going back to Library Menu...");
        }
    }
    

    public static void inventory() throws IOException{
        System.out.println(uc.getCurrentUser().inventoryString(in));

        profileMenu();
    }
    public static void cartMenu() throws IOException {
        boolean exitCartMenu = false;
        while (!exitCartMenu) {
            System.out.println(uc.getCurrentUser().cartString(in));
            System.out.println("\u001B[33mUser's Points: " + uc.getCurrentUser().getPoints() + "\u001B[0m");
            System.out.println();

            System.out.println("+-----------------+\r\n" +
                    "|  1     Pay      |\r\n" +
                    "|  2  Delete Cart |\r\n" +
                    "|  3   Go Back    |\r\n" + 
                    "+-----------------+");
    
            Scanner k = new Scanner(System.in);
            System.out.print("Enter your choice: ");
    
            boolean validInput = false;
            while (!validInput) {
                String userInput = k.nextLine();
                switch (userInput) {
                    case "1":
                        System.out.println("You selected Pay.");
                        exitCartMenu = true;
                        uc.getCurrentUser().pay(in);
                        fdl.saveListOfUsers(uc.getUsers()); //saves file
                        validInput = true;
                        cartMenu();
                        break;
                    case "2":
                        System.out.println("You selected Delete Cart.");
                        exitCartMenu = true;
                        uc.getCurrentUser().deleteCart();
                        validInput = true;
                        cartMenu();
                        break;
                    case "3":
                        System.out.println("Going back to the Profile Menu.");
                        exitCartMenu = true;
                        profileMenu();
                        validInput = true;
                        break;
                    default:
                        System.out.println("Invalid option. Please enter either 1, 2, or 3.");
                        System.out.print("Enter your choice again: ");
                        break;
                }
            }
        }
    }

    public static void connectSession(){ //(v3) Connects to the session
        logo();
        Scanner key = new Scanner(System.in);
        System.out.println("What is your username?");
        String name = key.next();
        System.out.println("What is your password?");
        String pass = key.next();
        uc.setCurrentUser(name, pass);
    }

    public static void newUser(){ //(v3) Asks user if he wants to login or create new login
        System.out.println( "\u001b[2J\u001b[H" ); //clears screen
        Scanner k = new Scanner(System.in);
        System.out.println("Do you wish to create an account y/n? (other inputs = no)");
        String answer = k.next();
        if(answer.matches("y")){
            boolean bool = true;
            String name = null;
            while(bool){
                bool=false;
                System.out.println("What username you wish to put?");
                name = k.next();
                for(User u : uc.getUsers()){ //checks if in the system there is a dupe name
                    if(name.equals(u.getName())){
                        System.out.println("Name already exists! Retry.");
                        bool = true;
                        break;
                    }

                }
            }
            System.out.println("What will be the password?");
            String pass = k.next();
            uc.createUser(name, pass);
        }
    }
    /*
     * METHODS FOR BOOKS AND INVENTORY CONTROLLER
     */

    public static void inventoryMenu() throws IOException{
        Scanner scan = new Scanner(System.in);

        System.out.println();
        System.out.println("\t       Admin Tools\r\n" + //
                "      (Books)              (Users)\r\n" + //
                "+-----------------+  +-----------------+\r\n" + //
                "|  1      Add     |  |  5    newAdmin  |\r\n" + //
                "|  2     Delete   |  |  6   viewHistory|\r\n" + //
                "|  3\t Modify   |  |  7     Delete   |\r\n" + //
                "|  4   ViewBooks  |  +-----------------+\r\n" + //
                "+-----------------+  |  8    Go Back   |");
                System.out.println("What would you like to do?");
        String input = scan.nextLine();

        while (!input.matches("[1-8]")) {
            System.out.println("Invalid choice. Please enter a valid option.");
            input = scan.nextLine();
        }
        int choice = Integer.parseInt(input);
        switch (choice) {
            case 1:
                createBook();
                System.out.println("Book added to the inventory!");
                inventoryMenu();
                break;
            case 2:
                deleteBook();
                System.out.println("Book deleted from the inventory!");
                inventoryMenu();
                break;
            case 3:
                modifyBook();
                System.out.println("Book modified from the inventory!");
                inventoryMenu();
                break;
            case 4:
                printBooksList();
                inventoryMenu();
                break;
                case 5:
                newAdmin();
            case 6:
                viewHistory();
            case 7:
                deleteUser();
            case 8:
                System.out.println("Exiting...");
                menu();
                break;
        }
    }
    public static void deleteUser() throws IOException {
        Scanner k = new Scanner(System.in);
        int cid = 0;
        do{
            System.out.println("What is the client's id? Use Numbers.");
            if (k.hasNextInt()) {
                cid = k.nextInt();
                break;
            } else {
                System.out.println("Invalid input. Please enter a valid integer.");
                k.next();
            }
        }while (true);

        boolean bool = false;

        for(User u : uc.getUsers()){
            if(u.getId() == cid){
                uc.deleteUser(cid);
                bool = true;
                break;
            }
        }
        if(bool){
            System.out.println("Removed User with id: " + cid);
        }
        else{
            System.out.println("Incorrect Client Id. Try Again.");
        }
        fdl.saveListOfUsers(uc.getUsers());
        inventoryMenu();
    }
    public static void viewHistory() throws IOException{
        Scanner k = new Scanner(System.in);
        int cid = 0;
        do{
            System.out.println("What is the client's id? Use Numbers.");
            if (k.hasNextInt()) {
                cid = k.nextInt();
                break;
            } else {
                System.out.println("Invalid input. Please enter a valid integer.");
                k.next();
            }
        }while (true);
        boolean bool = false;
        User u1 = null;
        for(User u : uc.getUsers()){
            if(u.getId() == cid){
                u1 = u;
                bool = true;
                break;
            }
        }
        if(bool){
            System.out.println("\u001b[2J\u001b[H");
            System.out.println(u1.historyString(in));
        }else{
            System.out.println("Incorrect Client Id. Try Again.");
        }
        inventoryMenu();
    }
    public static void newAdmin() throws IOException {
        Scanner k = new Scanner(System.in);
        int cid = 0;
        do{
            System.out.println("What is the client's id? Use Numbers.");
            if (k.hasNextInt()) {
                cid = k.nextInt();
                break;
            } else {
                System.out.println("Invalid input. Please enter a valid integer.");
                k.next();
            }
        }while (true);
        boolean bool = false;
        for(User u : uc.getUsers()){
            if(u.getId() == cid){
                bool = true;
                break;
            }
        }
        System.out.println("What will be the role of the selected Client?");
        String role = k.next();
        if(bool){
            uc.turnAdmin(cid, role);
            fdl.saveListOfUsers(uc.getUsers());
            System.out.println("Successfully converted Client to Admin");
        }else{
            System.out.println("Incorrect Client Id. Try Again.");
        }
        inventoryMenu();
    }

    public static void createBook() throws IOException {
        Scanner scan = new Scanner(System.in);
        InventoryController ic = new InventoryController();

        int lastUsedId = 0;
        int id;

        try {
            lastUsedId = InventoryController.getLastUsedId();
        } catch (IOException e) {
            e.printStackTrace();
        }

        id = lastUsedId + 1;

        String title;

        System.out.println(); // consume line
        do {
            System.out.println("What's the name of the book? Please use letters.");
            title = scan.nextLine();
        } while (!InventoryController.isValidString(title));

        String authorName;
        do {
            System.out.println("What's the name of the author? Please use letters.");
            authorName = scan.nextLine();
        } while (!InventoryController.isValidString(authorName));

        String publisherName;
        do {
            System.out.println("What's the name of the publisher? Please use letters.");
            publisherName = scan.nextLine();
        } while (!InventoryController.isValidString(publisherName));

        String category;
        do {
            System.out.println("What's the category of the book? Please use letters.");
            category = scan.nextLine();
        } while (!InventoryController.isValidString(category));

        String description;
        do {
            System.out.println("What's the description of the book? Please use letters.");
            description = scan.nextLine();
        } while (!InventoryController.isValidString(description));

        String language;
        do {
            System.out.println("What's the language of the book? Please use letters.");
            language = scan.nextLine();
        } while (!InventoryController.isValidString(language));

        int rating;
        do {
            System.out.println("What's the rating of the book? Integers between 0 and 10.");
            while (!scan.hasNextInt()) {
                System.out.println("Please enter a valid integer for the rating.");
                scan.next(); // consume the invalid input
            }
            rating = scan.nextInt();
            scan.nextLine(); // consume the newline character
        } while (rating < 0 || rating > 10);

        int price;
        do {
            System.out.println("What's the price of the book? Integers bigger than 0.");
            while (!scan.hasNextInt()) {
                System.out.println("Please enter a valid integer for the price.");
                scan.next(); // consume the invalid input
            }
            price = scan.nextInt();
            scan.nextLine(); // consume the newline character
        } while (price < 0);

        int answer;
        do {
            System.out.println("Is the book an ebook(1) or an audiobook?(2)");
            while (!scan.hasNextInt()) {
                System.out.println("Please enter a valid integer for the answer.");
                scan.next(); // consume the invalid input
            }
            answer = scan.nextInt();
            scan.nextLine(); // consume the newline character
        } while (answer != 1 && answer != 2);

        if (answer == 2){
            int duration;
            do {
                System.out.println("What's the duration of the audiobook?");
                while (!scan.hasNextInt()) {
                    System.out.println("Please enter a valid integer for the duration.");
                    scan.next(); // consume the invalid input
                }
                duration = scan.nextInt();
                scan.nextLine(); // consume the newline character
            } while (duration < 0);

            String narrator;
            do {
                System.out.println("Who's the narrator of the audiobook? Please use letters.");
                narrator = scan.nextLine();
            } while (!InventoryController.isValidString(narrator));

            Books book = new AudioBooks(id, title, authorName, publisherName, category, description, language, rating, price, duration, narrator);
            ic.addToAllBooksList(book);

            /* PRINT FIELDS OF THE BOOK CREATED */ /* 
            for (Books b : fullBook) {
                System.out.println(b.toString());
            }
            */
            
        }
        else{
            Books book = new Ebooks(id, title, authorName, publisherName, category, description, language, rating, price);
            ic.addToAllBooksList(book);
        }

        
    }

    public static void deleteBook() throws IOException {
        Scanner scan = new Scanner(System.in);
        InventoryController ic = new InventoryController();

        int bookID = 0;

        while (true) {
            System.out.println("Which book would you like to delete. Please type its ID:");
            
            if (scan.hasNextInt()) {
                bookID = scan.nextInt();
                break;
            } 
            else {
                System.out.println("Invalid input. Please enter a valid integer ID.");
                scan.next(); // Consume INVALID input
            }
        }
        System.out.println("You entered book ID: " + bookID);

        ic.deleteFromAllBooksList(bookID);
    }

    public static void modifyBook() throws IOException {
        Scanner scan = new Scanner(System.in);
        InventoryController ic = new InventoryController();
        FileDataLoader fdl = new FileDataLoader();

        int bookID = 0;

        while (true) {
            System.out.println("Which book would you like to modify. Please type its ID:");
            
            if (scan.hasNextInt()) {
                bookID = scan.nextInt();
                break;
            } 
            else {
                System.out.println("Invalid input. Please enter a valid integer ID.");
                scan.next(); // Consume INVALID input
            }
        }
        System.out.println("You entered book ID: " + bookID);

        List<Books> allBooksList = fdl.loadListOfBooks(); 

        // Find the book with the ID given
        Books oldBook = null;
        for (Books book : allBooksList) {
            if (book.getId() == bookID) {
                oldBook = book;
                break;
            }
        }

        if (oldBook != null) {
            // Create the new modified book
            Books newBook = createModifiedBook(bookID);
            // Modify the book in the inventory
            ic.modifyAllBooksList(oldBook, newBook);
        } 
        else {
            BookstoreApplication.bookNotFound();
        }

        
    }

    public static Books createModifiedBook(int bookID){   
        Scanner scan = new Scanner(System.in);
        // Validate title
        String title;

        do {
            System.out.println("What's the name of the book? Please use letters.");
            title = scan.nextLine();
        } while (!InventoryController.isValidString(title));
    
        // Validate authorName
        String authorName;
        do {
            System.out.println("What's the name of the author? Please use letters.");
            authorName = scan.nextLine();
        } while (!InventoryController.isValidString(authorName));
    
        // Validate publisherName
        String publisherName;
        do {
            System.out.println("What's the name of the publisher? Please use letters.");
            publisherName = scan.nextLine();
        } while (!InventoryController.isValidString(publisherName));
    
        // Validate category
        String category;
        do {
            System.out.println("What's the category of the book? Please use letters.");
            category = scan.nextLine();
        } while (!InventoryController.isValidString(category));
    
        // Validate description
        String description;
        do {
            System.out.println("What's the description of the book? Please use letters.");
            description = scan.nextLine();
        } while (!InventoryController.isValidString(description));
    
        // Validate language
        String language;
        do {
            System.out.println("What's the language of the book? Please use letters.");
            language = scan.nextLine();
        } while (!InventoryController.isValidString(language));
    
        // Validate rating
        int rating;
        do {
            System.out.println("What's the rating of the book? Integers between 0 and 10.");
            while (!scan.hasNextInt()) {
                System.out.println("Please enter a valid integer for the rating.");
                scan.next(); // consume the invalid input
            }
            rating = scan.nextInt();
            scan.nextLine(); // consume the newline character
        } while (rating < 0 || rating > 10);
    
        // Validate price
        int price;
        do {
            System.out.println("What's the price of the book? Integers bigger than 0.");
            while (!scan.hasNextInt()) {
                System.out.println("Please enter a valid integer for the price.");
                scan.next(); // consume the invalid input
            }
            price = scan.nextInt();
            scan.nextLine(); // consume the newline character
        } while (price < 0);
    
        int answer;
        do {
            System.out.println("Is the book an ebook(1) or an audiobook?(2)");
            while (!scan.hasNextInt()) {
                System.out.println("Please enter a valid integer for the answer.");
                scan.next(); // consume the invalid input
            }
            answer = scan.nextInt();
            scan.nextLine(); // consume the newline character
        } while (answer != 1 && answer != 2);
    
        if (answer == 2){
            int duration;
            do {
                System.out.println("What's the duration of the audiobook?");
                while (!scan.hasNextInt()) {
                    System.out.println("Please enter a valid integer for the duration.");
                    scan.next(); // consume the invalid input
                }
                duration = scan.nextInt();
                scan.nextLine(); // consume the newline character
            } while (duration < 0);
    
            String narrator;
            do {
                System.out.println("Who's the narrator of the audiobook? Please use letters.");
                narrator = scan.nextLine();
            } while (!InventoryController.isValidString(narrator));
    
            return new AudioBooks(bookID, title, authorName, publisherName, category, description, language, rating, price, duration, narrator);
        } 
        else {
            return new Ebooks(bookID, title, authorName, publisherName, category, description, language, rating, price);
        }
    }

    public static void bookNotFound() throws IOException {
        System.out.println("Sorry, that book was not found in our inventory.");
        inventoryMenu();
    }

    public static void printBooksList() throws IOException {
        FileDataLoader fdl = new FileDataLoader();
    
        List<Books> books = fdl.loadListOfBooks();
    
        for (int i = 0; i < books.size(); i++) {
            Books b = books.get(i);
    
            if (b instanceof Ebooks) {
                Ebooks ebook = (Ebooks) b;
                System.out.printf("ID: %d, Title: %s, Author: %s, Publisher: %s, Category: %s, Description: %s, Language: %s, Rating: %s, Price: %s. %n%n",
                        ebook.getId(), ebook.getTitle(), ebook.getAuthor(), ebook.getPublisher(), ebook.getCategory(),
                        ebook.getDescription(), ebook.getLanguage(), ebook.getRating(), ebook.getPrice());
            } 
            else if (b instanceof AudioBooks) {
                AudioBooks audiobook = (AudioBooks) b;
                System.out.printf("ID: %d, Title: %s, Author: %s, Publisher: %s, Category: %s, Description: %s, Language: %s, Rating: %s, Price: %s, Duration: %s, Narrator: %s. %n%n",
                        audiobook.getId(), audiobook.getTitle(), audiobook.getAuthor(), audiobook.getPublisher(), audiobook.getCategory(),
                        audiobook.getDescription(), audiobook.getLanguage(), audiobook.getRating(), audiobook.getPrice(),
                        audiobook.getDuration(), audiobook.getNarrator());
            }
        }

        /*
         * %s is a placeholder for a string
         * %n at the end is for a newline
         * 
         */
    }

    public static void printBooksListFiltered(List<Books> books) {
        for (int i = 0; i < books.size(); i++) {
            Books b = books.get(i);
    
            if (b instanceof Ebooks) {
                Ebooks ebook = (Ebooks) b;
                System.out.printf("ID: %d, Title: %s, Author: %s, Publisher: %s, Category: %s, Description: %s, Language: %s, Rating: %s, Price: %s. %n%n",
                        ebook.getId(), ebook.getTitle(), ebook.getAuthor(), ebook.getPublisher(), ebook.getCategory(),
                        ebook.getDescription(), ebook.getLanguage(), ebook.getRating(), ebook.getPrice());
            } 
            else if (b instanceof AudioBooks) {
                AudioBooks audiobook = (AudioBooks) b;
                System.out.printf("ID: %d, Title: %s, Author: %s, Publisher: %s, Category: %s, Description: %s, Language: %s, Rating: %s, Price: %s, Duration: %s, Narrator: %s. %n%n",
                        audiobook.getId(), audiobook.getTitle(), audiobook.getAuthor(), audiobook.getPublisher(), audiobook.getCategory(),
                        audiobook.getDescription(), audiobook.getLanguage(), audiobook.getRating(), audiobook.getPrice(),
                        audiobook.getDuration(), audiobook.getNarrator());
            }
        }
    }
}
