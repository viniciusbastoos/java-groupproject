package com.example;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class InventoryController{
    Scanner scan = new Scanner(System.in);

    static List<Books> allBooksList = new ArrayList<>();

    FileDataLoader fdl = new FileDataLoader();

    public InventoryController() throws IOException {
        InventoryController.allBooksList = fdl.loadListOfBooks();
    }

    /* 
     * This method is to add the book to an array and call saveListOfBooks
     */
    public void addToAllBooksList(Books book) throws IOException {

        List<Books> allBooksList = new ArrayList<Books>();

        allBooksList.add(book);

        try {
            fdl.saveListOfBooks(allBooksList);
        } 
        catch (IOException e) {
            e.printStackTrace();
        }

        BookstoreApplication.inventoryMenu();
    }
    public void deleteFromAllBooksList(int bookID) throws IOException {
        List<Books> allBooksList = fdl.loadListOfBooks(); 
        Books bookToRemove = null;

        for (Books book : allBooksList) {
            if (book.getId() == bookID) {
                bookToRemove = book;
                break;  // Found the book, no need to continue searching
            }
        }
    
        // If the book is found, remove it
        if (bookToRemove != null) {
            allBooksList.remove(bookToRemove);    
            reassignBookIDs(allBooksList);
        } 
        else {
            BookstoreApplication.bookNotFound();
            return;
        }
    
        try {
            fdl.updateTextFile(allBooksList);

        } 
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void modifyAllBooksList(Books oldBook, Books newBook) throws IOException {
        // Load the current inventory
        List<Books> allBooksList = fdl.loadListOfBooks();
    
        // Find the index of the old book in the list
        int index = -1;
        for (int i = 0; i < allBooksList.size(); i++) {
            if (allBooksList.get(i).getId() == oldBook.getId()) {
                index = i;
                break;
            }
        }
    
        // If the old book is found, replace it with the new book
        if (index != -1) {
            allBooksList.set(index, newBook);
    
            // Update the file with the modified inventory
            fdl.updateTextFile(allBooksList);
        } 
        else {
            BookstoreApplication.bookNotFound();
        }
    }

    
    public void reassignBookIDs(List<Books> allBooksList) {
        for (int i = 0; i < allBooksList.size(); i++) {
            allBooksList.get(i).setId(i + 1);
        }
    }

    public static int getLastUsedId() throws IOException {
        File file = new File("javaproject/data/books.txt");
    
        // If the file doesn't exist or is empty i want to return 0
        if (!file.exists() || file.length() == 0) {
            return 0;
        }
    
        // Read the file
        Path path = Paths.get("javaproject/data/books.txt");
        List<String> allLines = Files.readAllLines(path);
    
        // Get the last line
        String lastLine = allLines.get(allLines.size() - 1);
    
        // Split the last line to get the ID
        String[] fields = lastLine.split(",");
        int lastUsedId = Integer.parseInt(fields[0].trim());
    
        return lastUsedId;
    }
    

    public static boolean isValidString(String input) {
        return input.matches("[a-zA-Z ]+"); // regex for letter and space
    }

    public List<Books> getBooks(){
        return allBooksList;
    }

    /* 
    @Override
    public void saveListOfObjects() throws IOException{
        
    }
    */
}
