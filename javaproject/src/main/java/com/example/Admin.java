package com.example;

import java.util.ArrayList;

public class Admin extends User{
    private int adminid;
    private String role;
    public Admin(int userid, String username, 
    String pass, String email, 
    int points, ArrayList<Integer> inventory,
    ArrayList<Integer> history, int id,String role){
        super(userid,username,pass,email,points,inventory,history);
        this.adminid = id;
        this.role = role;
    }
    public int getAdmin(){return this.adminid;}
    public String getRole(){return this.role;}
    @Override
    public String toString(){
        String txt = this.getId() + ";" + this.getName() + ";" + this.getPass() + ";" +this.getMail() + ";" + this.getPoints() + ";";
        for (int i = 0; i < this.getInventory().size(); i++) {
            txt += this.getInventory().get(i);
            if (i < this.getInventory().size() - 1) {
                txt += ",";
            }
        }
        txt+=";";
        for (int i = 0; i < this.getHistory().size(); i++) {
            txt += this.getHistory().get(i);
            if (i < this.getHistory().size() - 1) {
                txt += ",";
            }
        }
        return txt+";"+this.adminid+";"+this.role;
    }
}
