package com.example;

import java.util.Comparator;

public class Ebooks extends Books{

    public Ebooks(int id,String title, String author, String publisher, 
                String category, String description, String language, 
                int rating, int price)
    {
        super(id, title, author, publisher, 
                category, description, language, 
                rating, price);
    }

    public String toString() {
        return getId() + ","
        + getTitle() + ","
        + getAuthor() + ","
        + getPublisher() + ","
        + getCategory() + ","
        + getDescription() + ","
        + getLanguage() + ","
        + getRating() + ","
        + getPrice() + ";";
    }

    public static Comparator<Books> idComparator = new Comparator<Books>() {
        @Override
        public int compare(Books book1, Books book2) {
            return Double.compare(book1.getId(), book2.getId());
        }
    };

    public static Comparator<Books> titleComparator = new Comparator<Books>() {
        @Override
        public int compare(Books book1, Books book2) {
            return book1.getTitle().compareTo(book2.getTitle());
        }
    };

    public static Comparator<Books> authorComparator = new Comparator<Books>() {
        @Override
        public int compare(Books book1, Books book2) {
            return book1.getAuthor().compareTo(book2.getAuthor());
        }
    };

    // Compare based on rating
    public static Comparator<Books> ratingComparator = new Comparator<Books>() {
        @Override
        public int compare(Books book1, Books book2) {
            return Double.compare(book1.getRating(), book2.getRating());
        }
    };

    // Compare based on price
    public static Comparator<Books> priceComparator = new Comparator<Books>() {
        @Override
        public int compare(Books book1, Books book2) {
            return Double.compare(book1.getPrice(), book2.getPrice());
        }
    };

    @Override
    public int compareTo(Books o) {
        return titleComparator.compare(this, o);
    }
}
