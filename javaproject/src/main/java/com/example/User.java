package com.example;
import java.util.ArrayList;
public abstract class User 
{
   private int userid;
   private String username;
   private String password;
   private String email;
   private int total; //temp variable
   private int points;
   private ArrayList<Integer> inventory;
   private ArrayList<Integer> history;
   private ArrayList<Integer> cart; //temp list

   public User(int userid, String username, String pass, String email, int points, ArrayList<Integer> inventory,ArrayList<Integer> history){
    this.userid = userid;
    this.username = username;
    this.password = pass;
    this.email = email;
    this.points = points;
    this.inventory = inventory;
    this.history = history;
    this.cart = new ArrayList<Integer>();
   }
   public boolean checkAdmin(){
      if(!(this instanceof Admin)){
          return false;
      }
      return true;
   }
   public String historyString(InventoryController in){
    String txt = this.username+"'s Past Purchases\n";
    for(int bookid:this.history){
        txt+="~ Book Id: "+bookid+" | Title: "+in.getBooks().get(bookid-1).getTitle()+" | Price: "+in.getBooks().get(bookid-1).getPrice()+"pts "+"\n";
    }
    return txt;
   }
   public String inventoryString(InventoryController in){
    String txt = this.username+"'s Current Inventory\n";
    for(int bookid:this.inventory){
        txt+="~ Book Id: "+bookid+" | Title: "+in.getBooks().get(bookid-1).getTitle()+" | Price: "+in.getBooks().get(bookid-1).getPrice()+"pts "+"\n";
    }
    return txt;
   }
   public String cartString(InventoryController in){
        total = 0;
        String txt = "*The discounts will be used automatically in the price if the points are bigger and equal*\n- 500pts = 50%off\n\n"+this.username+"'s Current Cart\n";
        if(cart.size() == 0){
            txt+="Nothing in the cart.";
            return txt;
        }
        for(int bookid:this.cart){
            total += discountPrice(in.getBooks().get(bookid-1).getPrice());
            txt+="~ Book Id: "+bookid+" | Title: "+in.getBooks().get(bookid-1).getTitle()+" | Price: "+discountPrice(in.getBooks().get(bookid-1).getPrice())+"pts\n";
        }
        return txt+"Total to pay: "+total+"pts";
    }
   public int discountPrice(int price){
        if(points >= 500){
            return price/2; //50%off
        }
        return price;
   }
   public void addToInventory(int bookid){
        inventory.add(bookid);
   }
   public void addToHistory(int bookid){
        history.add(bookid);
   }
   public void addToCart(int bookid){
        cart.add(bookid);
   }
   public void deleteCart(){
        total = 0;
        cart.clear(); 
    }

   public void pay(InventoryController inventoryController) {
    if (cart.isEmpty()) {
        System.out.println("Nothing to pay for. Cart is empty.");
        return;
    }
    int totalCost = 0;
    for (int bookId : cart) {
        totalCost += discountPrice(inventoryController.getBooks().get(bookId - 1).getPrice());
    }
    if (points >= totalCost) {
        points -= totalCost;

        history.addAll(cart);
        inventory.addAll(cart);
        
        cart.clear();

        System.out.println("Payment successful. " + totalCost + " points deducted.");
    } else {
        System.out.println("Insufficient points to make the payment.");
    }
}

   public void winPoints(){this.points += 2;} //when user logged in
   public void setName(String name){this.username = name;}
   public void setPass(String pass){this.password = pass;}
   public void setEmail(String email){this.email = email;}
   public int getId(){return this.userid;}
   public String getName(){return this.username;}
   public String getPass(){return this.password;}
   public String getMail(){return this.email;}
   public double getTotal(){return this.total;}
   public int getPoints(){return this.points;}
   public ArrayList<Integer> getInventory(){return this.inventory;}
   public ArrayList<Integer> getHistory(){return this.history;}
   public ArrayList<Integer> getCart(){return this.cart;}
   public abstract String toString();
}
