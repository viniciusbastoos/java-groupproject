package com.example;
import java.util.ArrayList;

public class Client extends User{
    public Client(int userid, String username, 
    String pass, String email, int points, 
    ArrayList<Integer> inventory, ArrayList<Integer> history){
        super(userid,username,pass,email,points,inventory,history);
    }
    @Override
    public String toString(){
        String txt = this.getId() + ";" + this.getName() + ";" + this.getPass() + ";" +this.getMail() + ";" + this.getPoints() + ";";
        for (int i = 0; i < this.getInventory().size(); i++) {
            txt += this.getInventory().get(i);
            if (i < this.getInventory().size() - 1) {
                txt += ",";
            }
        }
        txt+=";";
        for (int i = 0; i < this.getHistory().size(); i++) {
            txt += this.getHistory().get(i);
            if (i < this.getHistory().size() - 1) {
                txt += ",";
            }
        }
        return txt;
    }
}
