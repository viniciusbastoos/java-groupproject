package com.example;

import java.util.Comparator;

public abstract class Books implements Comparable<Books> {    
    int id;
    String title;
    String author;
    String publisher;
    String category;
    String description;
    String language;
    int rating;
    int price;

    public Books(int id,String title, String author, String publisher, 
                String category, String description, String language, 
                int rating, int price)
    {
        this.id = id;
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.category = category;
        this.description = description;
        this.language = language;
        this.rating = rating;
        this.price = price;
    }
    public int getId(){
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getTitle(){
        return title;
    }
    public void setTitle(String title){
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }
    public void setAuthor(String author){
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }
    public void setPublisher(String publisher){
        this.publisher = publisher;
    }

    public String getCategory() {
        return category;
    }
    public void setCategory(String category){
        this.category = category;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description){
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }
    public void setLanguage(String language){
        this.language = language;
    }

    public int getRating() {
        return this.rating;
    }
    public void setRating(int rating){
        if (rating >= 0 && rating <= 10) {
            this.rating = rating;
        }
    }

    public int getPrice() {
        return price;
    }
    public void setPrice(int price){
        if (price > 0) {
            this.price = price;
        }
    }

    public static Comparator<Books> idComparator = new Comparator<Books>() {
        @Override
        public int compare(Books book1, Books book2) {
            return Double.compare(book1.getId(), book2.getId());
        }
    };

    public static Comparator<Books> titleComparator = new Comparator<Books>() {
        @Override
        public int compare(Books book1, Books book2) {
            return book1.getTitle().compareTo(book2.getTitle());
        }
    };

    public static Comparator<Books> authorComparator = new Comparator<Books>() {
        @Override
        public int compare(Books book1, Books book2) {
            return book1.getAuthor().compareTo(book2.getAuthor());
        }
    };

    // Compare based on rating
    public static Comparator<Books> ratingComparator = new Comparator<Books>() {
        @Override
        public int compare(Books book1, Books book2) {
            return Double.compare(book1.getRating(), book2.getRating());
        }
    };

    // Compare based on price
    public static Comparator<Books> priceComparator = new Comparator<Books>() {
        @Override
        public int compare(Books book1, Books book2) {
            return Double.compare(book1.getPrice(), book2.getPrice());
        }
    };
    

    @Override
    public int compareTo(Books o) {
        // Use the title comparator by default
        return titleComparator.compare(this, o);
    }
}